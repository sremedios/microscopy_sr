'''
Running the forward pass of the SR algorithm on 
LSFM data is non-trivial due to its size.

We first assume that all the through-plane LR slices are
saved to disk in two separate folders (for each through-plane
orthogonal to the in-plane).

vvvv This script vvvv
Second, we run the model as chunks over the through-plane slices,
and save all of these slices to their respective directories.
This results in two more directories containing high-resolution
through-plane slices.
^^^^ This script ^^^^

Third, we want to reconstruct the in-plane slices and save them
in a final output directory. This is a multi-step process and is
done in a different script from this one.
'''
import argparse
from pathlib import Path
import sys
import time

import nibabel as nib
import numpy as np
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm
from PIL import Image

from .models.net import Net
from .utils.lsfm_test_loader import TestSet


def main(args=None):
    main_st = time.time()
    #################### ARGUMENTS ####################

    parser = argparse.ArgumentParser()
    parser.add_argument('--in-dir0', type=str, required=True)
    parser.add_argument('--in-dir1', type=str, required=True)
    parser.add_argument('--out-dir0', type=str, required=True)
    parser.add_argument('--out-dir1', type=str, required=True)
    parser.add_argument('--weight-dir', type=str, required=True)
    parser.add_argument('--gpu-id', type=int, default=0)
    parser.add_argument('--interp-order', type=int, default=5)
    parser.add_argument('--patch-size', type=int, default=32)
    parser.add_argument('--n-layers', type=int, default=32)
    parser.add_argument('--n-filters', type=int, default=256)
    parser.add_argument('--kernel-size', type=int, default=3)
    parser.add_argument('--acq-dim', type=int, choices=[2, 3], default=2)
    parser.add_argument('--chunk-size', type=int, default=None)
    parser.add_argument('--slice-spacing', type=float)
    parser.add_argument('--inplane-res', type=float)
    parser.add_argument('--partition', type=int)

    args = parser.parse_args(args if args is not None else sys.argv[1:])

    mode = 'SR'

    print('\n{} BEGIN PREDICTION {}\n'.format('=' * 20, '=' * 20))

    weight_dir = Path(args.weight_dir)

    #################### LOAD AND PROCESS DATA ####################
    ds = TestSet(
        img_dir0=Path(args.in_dir0),
        img_dir1=Path(args.in_dir1),
        slice_spacing=args.slice_spacing,
        inplane_res=args.inplane_res,
        partition=args.partition,
    )

    #################### PREDICT ####################

    gpu_device = torch.device(f'cuda:{args.gpu_id}')
    # Find the best validation loss weights in the directory

    weight_fpath = sorted(
        filter(lambda x: "weights" in x.name, weight_dir.iterdir()))[-1]
    print("Running model with weights:", str(weight_fpath))

    checkpoint = torch.load(weight_fpath)

    model = Net(
        n_channels=1,
        patch_size=args.patch_size,
        kernel_size=args.kernel_size,
        n_layers=args.n_layers,
        filters=args.n_filters,
    )
    model.load_state_dict(checkpoint['model'])
    model.to(gpu_device).eval()

    #################### APPLY MODEL ####################

    print('{} Applying {} network... {}'.format('=' * 20, mode, '=' * 20))

    for i in tqdm(range(len(ds))):
        # TODO: ONLY RUNNING ON ONE SLICE TO TUNE!
        # TODO: ONLY RUNNING ON ONE SLICE TO TUNE!
        # if i != 60:
        # continue
        # TODO: ONLY RUNNING ON ONE SLICE TO TUNE!
        # TODO: ONLY RUNNING ON ONE SLICE TO TUNE!
        out_fname = f'{(i + ds.idx_offset)*10:06d}.tif'

        x0, x1 = ds.__getitem__(i)
        with torch.set_grad_enabled(False):
            target_shape = x0.shape
            # Pre-pad the image (will be reduced in size after
            # running through the network
            pad_amt = int((args.chunk_size - model.pred_fov) / 2)
            pads = ((pad_amt, pad_amt), (pad_amt, pad_amt))

            for x, outdir in zip([x0, x1], [Path(args.out_dir0), Path(args.out_dir1)]):
                if not outdir.exists():
                    outdir.mkdir(parents=True)
                x = np.pad(x, pads, mode='reflect')
                # Run model
                slice_pred = model.forward_in_chunks(
                    x,
                    chunk_size=args.chunk_size,
                    target_shape=target_shape,
                    device=gpu_device,
                )
                # save slice_pred to file
                Image.fromarray(slice_pred).save(
                    str(outdir / out_fname), "TIFF")

    main_en = time.time()
    print('\n\nDONE\nElapsed time: {:.4f}s\n'.format(main_en - main_st))
    print('\n{} END PREDICTION {}\n'.format('=' * 20, '=' * 20))
