import torch
from torch import nn
import numpy as np


class ResBlock(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        res_scale,
    ):
        super(ResBlock, self).__init__()

        self.res_scale = res_scale
        self.kernel_size = kernel_size

        '''
        self.match_feats = nn.Conv2d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=1,
            stride=1,
            bias=True,
        )
        '''

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                bias=True,
            ),
            nn.ReLU(inplace=True),
            nn.Conv2d(
                in_channels=out_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                bias=True,
            ),
        )

    def forward(self, x):
        c = int(np.floor(self.kernel_size / 2)) * 2
        out = self.body(x).mul(self.res_scale)
        x_crop = x[:, :, c:-c, c:-c]

        '''
        # match the features for the output
        x_match_feats = self.match_feats(x_crop)
        return out + x_match_feats
        '''
        return out + x_crop


class ConvAct(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        stride=1,
        bias=True,
        activation=nn.ReLU(inplace=True),
    ):
        super(ConvAct, self).__init__()
        self.conv = nn.Conv2d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=kernel_size,
            stride=stride,
            bias=bias,
        )
        self.act = activation

    def forward(self, x):
        return self.act(self.conv(x))


class Net(nn.Module):
    def __init__(
        self,
        n_channels,
        patch_size=64,
        kernel_size=3,
        n_layers=32,
        filters=256,
    ):
        super(Net, self).__init__()
        self.pred_fov = int(
            patch_size - (2 * np.floor(kernel_size / 2) * n_layers)
        )

        self.kernel_size = kernel_size
        self.n_layers = n_layers

        self.head = ConvAct(
            in_channels=n_channels,
            out_channels=filters,
            kernel_size=kernel_size,
            stride=1,
            bias=True,
            activation=nn.ReLU(inplace=True),
        )

        # We repeat ResBlocks for `(n_layers - 2) / 2` iterations.
        # Subtract 2 bc head and tail are each one conv
        # Divide by 2 bc each resblock has two layers
        self.n_resblocks = (n_layers - 2) // 2
        self.body = nn.Sequential(
            *[
                ResBlock(
                    in_channels=filters,
                    out_channels=filters,
                    kernel_size=kernel_size,
                    res_scale=0.1,
                ) for _ in range(self.n_resblocks)
            ],
        )

        # tail convolution
        self.tail = nn.Conv2d(
            in_channels=filters,
            out_channels=n_channels,
            kernel_size=kernel_size,
            stride=1,
            bias=True,
        )

    def forward(self, x):
        x = self.head(x)
        res = self.body(x)
        # cropping by 2 for each layer in the resblocks
        c = int(self.n_resblocks * 2 * np.floor(self.kernel_size / 2))
        x_crop = x[:, :, c:-c, c:-c]
        out = self.tail(res + x_crop)
        return out

    def forward_in_chunks(self, x, chunk_size, target_shape, device):
        out = np.zeros(target_shape, dtype=x.dtype)

        for i in range(0, x.shape[0], self.pred_fov):
            for j in range(0, x.shape[1], self.pred_fov):
                chunk = x[i:i+chunk_size, j:j+chunk_size]

                pads = [(np.abs(a - b) // 2, np.abs(a - b) // 2)
                        for a, b in zip(chunk.shape, (chunk_size, chunk_size))]
                crops = [slice(p[0], -p[1]) for p in pads]
                crops = [c if c != slice(0, 0) else slice(
                    None, None) for c in crops]
                crops = tuple(crops)
                # Pad chunk if necessary
                chunk = np.pad(chunk, pads, mode='reflect')
                # Run network
                chunk_pred = self.forward(
                    torch.from_numpy(chunk).to(device).permute(
                        1, 0).unsqueeze(0).unsqueeze(1)
                ).detach().cpu().squeeze().permute(1, 0).numpy()

                # Crop if necessary
                chunk_pred = chunk_pred[crops]
                # Concatenate results to the right spot
                out[i:i+self.pred_fov, j:j+self.pred_fov] = chunk_pred
        return out
