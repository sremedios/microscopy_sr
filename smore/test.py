import argparse
from pathlib import Path
import sys
import time

import nibabel as nib
import numpy as np
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm

from .models.net import Net
from .models.edsr import EDSR
from .utils.fba import fba
from .utils.pad import crop
from .utils.preprocess import open_interp_rotate, reorient
from .utils.rotate import rotate_vol_2d, inv_rotate_vols_2d


def apply_to_images(data_list, mode, model_type, weight_dir, orig_min, cli_args):
    #################### MODEL SETUP ####################
    gpu_device = torch.device(f'cuda:{cli_args.gpu_id}')
    if cli_args.weight_file is not None:
        checkpoint = torch.load(cli_args.weight_file)
    else:
        if cli_args.weight_step is None:
            # Find the best validation loss weights in the directory

            weight_fpath = sorted(
                filter(lambda x: mode in x.name, weight_dir.iterdir()))[-1]
            print("Running model with weights:", str(weight_fpath))

            checkpoint = torch.load(weight_fpath)

        else:
            checkpoint = torch.load(
                weight_dir / f'step_{cli_args.weight_step}_{mode}_weights.h5')

    if model_type == 'EDSR':
        model = EDSR(
            n_channels=1,
            patch_size=cli_args.patch_size,
            kernel_size=cli_args.kernel_size,
            n_resblocks=int((cli_args.n_layers - 2) / 2),
            filters=cli_args.n_filters,
            padding_mode='zeros',
            min_clip=0,
        )
    else:
        model = Net(
            n_channels=1,
            patch_size=cli_args.patch_size,
            kernel_size=cli_args.kernel_size,
            n_layers=cli_args.n_layers,
            filters=cli_args.n_filters,
        )
    model.load_state_dict(checkpoint['model'])
    model.to(gpu_device).eval()

    #################### APPLY MODEL ####################

    print('{} Applying {} network... {}'.format('=' * 20, mode, '=' * 20))
    vols = [[] for _ in range(cli_args.n_rots)]
    for img_num, vol in enumerate(data_list):
        with torch.set_grad_enabled(False):
            for img in tqdm(vol):
                target_shape = img.shape
                # Pre-pad the image
                pad_amt = int((cli_args.chunk_size - model.pred_fov) / 2)
                # Hacky code just to compare for full slice condition:
                # Just pad by 16 at image borders
                if pad_amt < 0:
                    pad_amt = 16
                pads = ((pad_amt, pad_amt), (pad_amt, pad_amt))
                crops = [slice(p[0], -p[1]) for p in pads]
                crops = [c if c != slice(0, 0) else slice(None, None)
                         for c in crops]
                crops = tuple(crops)

                img = np.pad(img, pads, mode='reflect')

                if cli_args.chunk_size is None or cli_args.chunk_size == 0:
                    slice_pred = model(
                        torch.from_numpy(img)
                        .permute(1, 0)
                        .unsqueeze(0)
                        .unsqueeze(1)
                        .to(gpu_device)
                    ).detach()\
                        .cpu()\
                        .squeeze()\
                        .permute(1, 0)\
                        .numpy()

                else:
                    slice_pred = model.forward_in_chunks(
                        img,
                        chunk_size=cli_args.chunk_size,
                        target_shape=target_shape,
                        device=gpu_device,
                    )

                # Our zero-padded network needs a crop
                # if model_type == 'EDSR':
                #slice_pred = slice_pred[crops]
                shape_diffs = [(int(np.abs(a - b) / 2), int(np.abs(a - b) / 2))
                               for a, b in zip(target_shape, slice_pred.shape)]
                crops = [slice(p[0], -p[1]) for p in shape_diffs]
                crops = [c if c != slice(0, 0) else slice(None, None)
                         for c in crops]
                crops = tuple(crops)
                slice_pred = slice_pred[crops]
                vols[img_num].append(slice_pred)
    # aggregate into volume
    vols = [np.array(vol) for vol in vols]

    # if model_type == 'EDSR':
    #raise NotImplementedError
    # return [rotate_vol_2d(vol, 90 if mode == 'AA' else 270) for vol in vols]
    # return [rotate_vol_2d(vol, 270) for vol in vols]
    return vols


def main(args=None):
    main_st = time.time()
    #################### ARGUMENTS ####################

    parser = argparse.ArgumentParser()
    parser.add_argument('--in-fpath', type=str, required=True)
    parser.add_argument('--out-fpath', type=str, required=True)
    parser.add_argument('--weight-dir', type=str, required=True)
    parser.add_argument('--weight-step', type=str, default=None)
    parser.add_argument('--weight-file', type=str, default=None)
    parser.add_argument('--gpu-id', type=int, default=0)
    parser.add_argument('--interp-order', type=int, default=5)
    parser.add_argument('--patch-size', type=int, default=32)
    parser.add_argument('--batch-size', type=int, default=1)
    parser.add_argument('--n-layers', type=int, default=32)
    parser.add_argument('--n-rots', type=int, default=4)
    parser.add_argument('--n-filters', type=int, default=256)
    parser.add_argument('--kernel-size', type=int, default=3)
    parser.add_argument('--acq-dim', type=int, choices=[2, 3], default=2)
    parser.add_argument('--no-init-interp', action='store_true', default=False)
    parser.add_argument('--lr-axis', type=int)
    parser.add_argument('--chunk-size', type=int, default=None)
    #parser.add_argument('--pad-amt', type=int, default=16)
    parser.add_argument('--model-type', type=str, required=True)

    args = parser.parse_args(args if args is not None else sys.argv[1:])

    print('\n{} BEGIN PREDICTION {}\n'.format('=' * 20, '=' * 20))

    out_dir = Path(args.out_fpath).parent
    weight_dir = Path(args.weight_dir)

    if not out_dir.exists():
        out_dir.mkdir(parents=True)

    #################### LOAD AND PROCESS DATA ####################

    angles = [0, 90, 180, 270][:args.n_rots]
    res = open_interp_rotate(
        img_fpath=args.in_fpath,
        n_rots=args.n_rots,
        angles=angles,
        lr_axis=args.lr_axis,
        acq_dim=args.acq_dim,
        init_interp=not args.no_init_interp,
        interp_order=args.interp_order,
    )
    imgs_rot, affine, header, pads, acq_res, k, blur_k, lr_axis = res

    orig_min = imgs_rot[0].min()

    #################### PREDICT ####################

    st = time.time()

    '''
    if args.model_type == 'EDSR':
        ##### Apply AA to all rotations #####
        aa_vols = apply_to_images(
            imgs_rot, 'AA', args.model_type, weight_dir, orig_min, args)
        ##### Apply SR to all rotations #####
        sr_vols = apply_to_images(
            aa_vols, 'SR', args.model_type, weight_dir, orig_min, args)
    else:
        ##### Apply SR to all rotations #####
        sr_vols = apply_to_images(
            imgs_rot, 'SR', args.model_type, weight_dir, orig_min, args)
    '''
    ##### Apply SR to all rotations #####
    sr_vols = apply_to_images(
        imgs_rot, 'SR', args.model_type, weight_dir, orig_min, args)
    # Rotate preds back
    model_preds = inv_rotate_vols_2d(sr_vols, angles=angles)

    # Save each pre-FBA output
    for i, model_pred in enumerate(model_preds):
        f = Path(args.out_fpath)
        out_fpath = f.parent / f.name.replace('.nii', f'_preFBA_{i}.nii')
        # Update header
        new_scales = [1, 1]
        new_scales.insert(lr_axis, 1/k)
        new_scales = tuple(new_scales)
        new_affine = np.matmul(affine, np.diag(new_scales + (1,)))
        # Write nifti
        out_obj = nib.Nifti1Image(
            model_pred,
            affine=new_affine,
            header=header,
        )
        nib.save(out_obj, out_fpath)

    en = time.time()
    print('Time for prediction : {:.4f}s'.format(en - st))

    #################### FBA ####################

    # Fourier Burst Accumulation
    st = time.time()

    # Only do FBA if there are multiple volumes on which to apply
    if len(model_preds) > 1:
        print('Applying FBA...')
        final_out = fba(model_preds, p=0)
    else:
        print('Only one rotation; skipping FBA.')
        final_out = model_preds[0]

    print('Pre-crop shape:', final_out.shape)

    # Re-crop to target shape
    final_out = crop(final_out, pads)

    print('Post-crop shape:', final_out.shape)

    # Reorient to original orientation
    final_out = reorient(final_out, lr_axis)

    en = time.time()
    print('Elapsed Time for final processing: {:.4f}s'.format(en - st))

    print('Saving image...')
    # Update header
    new_scales = [1, 1]
    new_scales.insert(lr_axis, 1/k)
    new_scales = tuple(new_scales)
    new_affine = np.matmul(affine, np.diag(new_scales + (1,)))
    # Write nifti
    out_obj = nib.Nifti1Image(
        final_out,
        affine=new_affine,
        header=header,
    )
    nib.save(out_obj, args.out_fpath)

    main_en = time.time()
    print('\n\nDONE\nElapsed time: {:.4f}s\n'.format(main_en - main_st))
    print('\tWritten to: {}\n'.format(args.out_fpath))
    print('\n{} END PREDICTION {}\n'.format('=' * 20, '=' * 20))
