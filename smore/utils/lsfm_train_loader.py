'''
For loading LSFM data stored in a directory
as a list of .tif files holding slices of the volume.


'''
from pathlib import Path
import numpy as np
from torch.utils.data import Dataset
import time
from PIL import Image
from tqdm import tqdm
import torch

from .augmentations import *
from .degrade import blur, alias, fwhm_needed
from .lsfm_patch_ops import *


class TrainSet(Dataset):
    def __init__(
            self,
            img_dir,
            patch_size,
            n_patches,
            acq_res,
            slice_spacing,
            n_super_patches=100,
            max_intensity_shift=100,
            pad_amt=16,
            split='train',
            n_rots=6,
            mode='SR',
            super_patch_freq=10000,
            verbose=True,
            augment=True,
            dtype=np.float32,
            interp_order=5,
            weighted_sampling=True,
            lr_axis=None,
            blur_kernel_file=None,
    ):
        '''
        If our patch size is p x p, we can hold 1000 super patches in RAM without being egregious.
        (1000 * (p * 10) * (p * 10) * 32) / 8e9 = 1.6 GB when p = 64
        and = 6.5 GB when p = 128

        But pulling 100 super patches takes about 40s, which is affordable. We'll do this
        with S frequency:

        Every S steps, we'll pull another `n_super_patches` super patches.

        This incurs the data loading cost; how often should we pull a new batch of super patches?
        If we do 320,000 total patches, then our time is:
        40s * 320000 / 1000 = 12800s = 213m = 3.5 hours!
        Instead if we pull our 100 super patches every 10,000, then it's 21.3 minutes.
        This time is clearly linear.
        Then pulling 100 super patches 3 times ~= every 100,000 patches and there's a 2 minute overhead
        to load.

        If S = `super_patch_freq` = 10000;
        ie: every 10000 steps, we'll pull another 1000 super patches.
        '''
        self.split = split
        self.n_super_patches = n_super_patches
        self.super_patch_freq = super_patch_freq
        self.acq_res = acq_res
        self.slice_thickness = fwhm_needed(min(acq_res), max(acq_res))
        self.slice_spacing = slice_spacing
        self.scale = slice_spacing / min(acq_res)
        self.max_intensity_shift = max_intensity_shift
        self.n_patches = n_patches
        self.mode = mode
        self.patch_size = patch_size
        self.augment = augment
        self.pad_amt = pad_amt
        self.slice_files = list(Path(img_dir).iterdir())

        self.super_patch_batch_counter = 0
        print("Collecting initial super patch batch...")
        self.super_patches = self.get_super_patch_batch()
        # we'll only get a number of centers based on the
        # fraction of total patches and the frequency of re-super-patch-batching
        self.centers = get_random_centers(
            self.super_patches,
            self.patch_size,
            n_patches=self.n_patches,
            pad_amt=self.pad_amt,
        )

    def __len__(self):
        return self.n_patches

    def set_mode(self, mode):
        self.mode = mode

    def get_super_patch_batch(self):
        '''
        Always hold some number of super patches in RAM for efficiency.
        Every so often, we'll pull new super patches from the entire dataset
        to see more data.
        '''
        super_patches = []
        for _ in tqdm(range(self.n_super_patches)):
            # randomly choose a slice in the directory
            slice_file = np.random.choice(self.slice_files)
            img = np.array(Image.open(slice_file), dtype=np.float32)

            # randomly choose a super-patch from the slice
            # (we do this so our patches don't suffer from boundary
            # effects when convolving
            super_patch_size = [10 * p for p in self.patch_size]
            super_patch_center_idx = index_by_gradient(
                img, super_patch_size, n_choices=1)[0]
            super_patch_center = [item[0] for item in np.unravel_index(
                [super_patch_center_idx], img.shape)]

            sts = [c - int(np.floor(p/2))
                   for c, p in zip(super_patch_center, super_patch_size)]
            ens = [st + p for st, p in zip(sts, super_patch_size)]

            idx = tuple(slice(st, en) for st, en in zip(sts, ens))

            super_patches.append(img[idx].copy())
        return super_patches

    def __getitem__(self, i):

        self.super_patch_batch_counter += 1
        # only have the first worker fetch a new super patch batch
        if torch.utils.data.get_worker_info() is not None:
            uid = torch.utils.data.get_worker_info().id
        else:
            uid = -1
        if uid == 0 and self.split == 'train' and self.super_patch_batch_counter > self.super_patch_freq:

            print("Collecting next super patch batch...")
            # pull more data
            self.super_patch_batch_counter = 0
            self.super_patches = self.get_super_patch_batch()
            self.centers = get_random_centers(
                self.super_patches,
                self.patch_size,
                n_patches=self.n_patches,
                pad_amt=self.pad_amt,
            )

        # select super patch and its patch center
        super_patch_i, center_idx = self.centers[i]
        super_patch = self.super_patches[super_patch_i]

        # Degrade
        super_patch_deg = blur(
            super_patch,
            blur_fwhm=self.slice_thickness,
            axis=0,
        )

        if self.slice_spacing != 1:
            super_patch_deg = alias(
                super_patch_deg,
                k=self.scale,
                down_order=1,
                up_order=5,
                axis=0,
            )

        center = [item[0]
                  for item in np.unravel_index([center_idx], super_patch.shape)]

        x, y = get_patch(super_patch, super_patch_deg, center, self.patch_size)

        # augment
        if self.augment:
            x, y = flip_xy(x, y)
            x, y = apply_intensity_shift(x, y, self.max_intensity_shift)
            x = add_noise(x, np.random.uniform(low=1, high=3))

        if self.pad_amt is not None:
            # crop FOV
            y = y[self.pad_amt:-self.pad_amt, self.pad_amt:-self.pad_amt]

        # return with added channel dim since this data is single-channel
        return x[np.newaxis, ...], y[np.newaxis, ...]
