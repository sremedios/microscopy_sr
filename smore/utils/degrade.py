import numpy as np
from resize.scipy import resize
from scipy import ndimage
from scipy.signal import windows


WINDOW_OPTIONS = ['blackman', 'hann', 'hamming', 'gaussian', 'cosine', 'parzen']


def std_to_fwhm(sigma):
    return 2 * np.sqrt(2 * np.log(2)) * sigma


def fwhm_to_std(gamma):
    return gamma / (2 * np.sqrt(2 * np.log(2)))


def fwhm_needed(fwhm_hr, fwhm_lr):
    return np.sqrt(fwhm_lr ** 2 - fwhm_hr ** 2)


def select_kernel(window_size, window_choice=None, fwhm=None, sym=True):
    if window_choice is None:
        window_choice = np.random.choice(WINDOW_OPTIONS)
    elif window_choice not in WINDOW_OPTIONS:
        raise ValueError('Window choice (%s) is not supported.' % window_choice)

    window = getattr(windows, window_choice)
    if window_choice in ['gaussian']:
        return window(window_size, fwhm_to_std(fwhm), sym)
    else:
        return window(window_size, sym)


def blur(x, blur_fwhm, axis, kernel_type='gaussian', kernel_file=None):
    if kernel_file is not None:
        kernel = np.load(kernel_file)
    else:
        window_size = int(2 * round(blur_fwhm) + 1)
        kernel = select_kernel(window_size, kernel_type, fwhm=blur_fwhm)
    kernel /= kernel.sum()  # remove gain
    blurred = ndimage.convolve1d(x, kernel, mode='nearest', axis=axis)

    return blurred


def alias(img, k, down_order, up_order, axis):
    dxyz_down = [1.0 for _ in img.shape]
    dxyz_down[axis] = k
    dxyz_up = [1.0 for _ in img.shape]
    dxyz_up[axis] = 1 / k

    img_ds = resize(img, dxyz=dxyz_down, order=down_order)
    img_us = resize(img_ds, dxyz=dxyz_up, order=up_order, target_shape=img.shape)

    return img_us
