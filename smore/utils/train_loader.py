import numpy as np
from torch.utils.data import Dataset
import time

from .augmentations import *
from .degrade import blur, alias
from .patch_ops import get_patch, get_random_centers
from .preprocess import open_interp_rotate


class TrainSet(Dataset):
    def __init__(
            self,
            img_fpath,
            patch_size,
            n_patches,
            model_type,
            pad_amt=16,
            split='train',
            n_rots=6,
            mode='SR',
            verbose=True,
            augment=True,
            dtype=np.float32,
            init_interp=True,
            interp_order=5,
            weighted_sampling=True,
            lr_axis=None,
            slice_thickness=None,
            slice_spacing=None,
            blur_kernel='gaussian',
            blur_kernel_file=None,
            acq_dim=2,
            iteration=1,
    ):
        self.n_patches = n_patches
        self.mode = mode
        self.patch_size = patch_size
        self.augment = augment
        self.model_type = model_type
        self.pad_amt = pad_amt
        # for subsequent iterations, don't upsample the volume
        if iteration > 1:
            self.init_interp = False
        else:
            self.init_interp = init_interp

        ##### Open and preprocess file #####
        st = 0.0
        if verbose:
            print('Opening image and creating rotations...')
            st = time.time()

        if n_rots == 1:
            angles = [0]
        else:
            angles = list(range(0, 91, 91 // (n_rots - 1)))

        if split == 'train':
            angles = angles[:-1]
        else:
            angles = angles[-1:]

        res = open_interp_rotate(
            img_fpath=img_fpath,
            n_rots=n_rots,
            angles=angles,
            dtype=dtype,
            verbose=verbose,
            init_interp=self.init_interp,
            interp_order=interp_order,
            lr_axis=lr_axis,
            slice_thickness=slice_thickness,
            slice_spacing=slice_spacing,
            acq_dim=acq_dim,
        )

        self.imgs_rot, affine, header, pads, self.acq_res, \
            self.downsample_ratio, self.blur_fwhm, lr_axis = res

        self.max_intensity_shift = get_intensity_shift(self.imgs_rot[0])

        if verbose:
            en = time.time()
            print('\tElapsed time: {:.4f}s'.format(en - st))

        ##### Blur #####
        if verbose:
            print('Blurring in-plane...')
            st = time.time()
        self.imgs_blur = [blur(img_rot, self.blur_fwhm, axis=0, kernel_type=blur_kernel,
                               kernel_file=blur_kernel_file)
                          for img_rot in self.imgs_rot]
        if verbose:
            en = time.time()
            print('\tElapsed time: {:.4f}s'.format(en - st))

        ##### Alias #####
        if verbose:
            print('Generating in-plane aliasing artifacts...')
            st = time.time()
        self.imgs_alias = [alias(img_blur, self.downsample_ratio, down_order=interp_order,
                                 up_order=interp_order, axis=0)
                           for img_blur in self.imgs_blur]
        if verbose:
            en = time.time()
            print('\tElapsed time: {:.4f}s'.format(en - st))

        ##### Calculate Random Centers #####
        if verbose:
            print('Generating (weighted) random patch centers...')
            st = time.time()
        self.centers = get_random_centers(self.imgs_rot, self.patch_size, self.n_patches,
                                          weighted=weighted_sampling)
        if verbose:
            en = time.time()
            print('\tElapsed time: {:.4f}s'.format(en - st))

    def __len__(self):
        return self.n_patches

    def set_mode(self, mode):
        self.mode = mode

    def __getitem__(self, i):
        rot_i, center_idx = self.centers[i]
        center = [item[0] for item in np.unravel_index(
            [center_idx], self.imgs_rot[rot_i].shape)]
        x, y = get_patch(
            self.imgs_rot[rot_i],
            self.imgs_blur[rot_i],
            self.imgs_alias[rot_i],
            center,
            self.patch_size,
            self.mode,
        )

        if self.augment:
            x, y = flip_xy(x, y)
            x, y = apply_intensity_shift(x, y, self.max_intensity_shift)

        # crop y since our network uses valid convs
        # hard-coded for ease: we train with 64x64 -> 32x32 patches
        # so we chop off 32/2 from both sides
        if self.model_type != 'EDSR':
            y = y[self.pad_amt:-self.pad_amt, self.pad_amt:-self.pad_amt]

        # add channel dim since this data is single-channel
        return x[np.newaxis, ...], y[np.newaxis, ...]
