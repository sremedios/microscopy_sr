import numpy as np
from scipy.ndimage import gaussian_filter


def get_patch(super_patch, super_patch_deg, patch_center, patch_size):

    # Get random rotation and center
    sts = [c - int(np.floor(p/2))
           for c, p in zip(patch_center, patch_size)]
    ens = [st + p for st, p in zip(sts, patch_size)]
    idx = tuple(slice(st, en) for st, en in zip(sts, ens))

    return super_patch_deg[idx], super_patch[idx]


def index_by_gradient(img, patch_size, n_choices, pad_amt=None):
    # slight smoothing to prevent noise from getting picked up
    # by gradient calculation
    smooth = gaussian_filter(img, 5.0)
    grads = np.gradient(smooth)
    grad_mag = np.sum([np.sqrt(np.abs(grad)) for grad in grads], axis=0)
    # zero out magnitude at borders
    grad_mag[:int(np.floor(patch_size[0] / 2)), :] = 0.0
    grad_mag[:, :int(np.floor(patch_size[1] / 2))] = 0.0
    grad_mag[-int(np.ceil(patch_size[0] / 2)):, :] = 0.0
    grad_mag[:, -int(np.ceil(patch_size[1] / 2)):] = 0.0

    # aggressive thresholding; we want to keep the sharpest
    # edges. We'll set all magnitudes less than the 95th
    # percentile to 0
    q = np.quantile(grad_mag, 0.9)
    grad_mag[grad_mag < q] = 0

    # If we're calculating this on the cropped super patch, then
    # we need to ensure the size is the same as the full super patch
    # to prevent out-of-bounds indexing after unraveling.
    if pad_amt is not None:
        grad_mag = np.pad(
            grad_mag,
            ((pad_amt, pad_amt), (pad_amt, pad_amt)),
            mode='constant',
            constant_values=0,
        )
    grad_probs = grad_mag.flatten()/grad_mag.sum()
    rand_idxs = np.random.choice(np.arange(0, np.prod(
        grad_mag.shape)), size=n_choices, p=grad_probs)
    return rand_idxs


def get_random_centers(super_patches, patch_size, n_patches, pad_amt):
    super_patch_choices = np.random.randint(
        0, len(super_patches), size=n_patches)
    centers = []
    for i, super_patch in enumerate(super_patches):
        # crop out the center based on the network's FOV;
        # we want the structure to exist AFTER the network
        # shrinks the image size
        cropped_super_patch = super_patch[pad_amt:-pad_amt, pad_amt:-pad_amt]
        n_choices = int(np.sum(super_patch_choices == i))
        rand_idxs = index_by_gradient(
            cropped_super_patch, patch_size, n_choices, pad_amt)
        centers.extend((i, int(center)) for center in rand_idxs)
    np.random.shuffle(centers)
    return centers
