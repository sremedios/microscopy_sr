import numpy as np
from torch.utils.data import Dataset


class TestSet(Dataset):
    def __init__(self, imgs):
        self.imgs = imgs
        self.img_nums_check = np.cumsum([img.shape[0] for img in self.imgs])

    def __len__(self):
        return sum([img.shape[0] for img in self.imgs])

    def __getitem__(self, idx):
        # get the index of the rotation image and the relative slice within that image
        img_num = list(map(lambda i: i > idx, self.img_nums_check)).index(True)
        rel_idx = idx - (0 if img_num ==
                         0 else self.img_nums_check[img_num - 1])

        img = self.imgs[img_num][rel_idx]

        # since we have a valid-conv network, we need to pad out
        # to retain the same size
        img = np.pad(img, ((16, 16), (16, 16)), mode='reflect')

        # add channel dim
        img = img[np.newaxis, ...]

        # return rotation index and slice with added channel dimension
        return img_num, img
