'''
This test-time loader assumes the through-plane
2D slices have already been created and exist
in two directories, both provided at construction time.

The loader simply sequentially loads a slice and interpolates
it to the correct grid size.
'''

from pathlib import Path
import numpy as np
from torch.utils.data import Dataset
from PIL import Image
from resize.scipy import resize


class TestSet(Dataset):
    def __init__(self, img_dir0, img_dir1, inplane_res, slice_spacing, partition, total_partitions=4):
        self.scale = slice_spacing / inplane_res

        self.fpaths0 = sorted(img_dir0.iterdir())[998:1200]
        self.fpaths1 = sorted(img_dir1.iterdir())[998:1200]

        # split up the work by partition, depending on how many partitions there are
        self.fpaths0 = np.array_split(
            self.fpaths0, total_partitions)[partition]
        self.fpaths1 = np.array_split(
            self.fpaths1, total_partitions)[partition]

        self.idx_offset = int(self.fpaths0[0].name.split('.')[0]) // 10

    def __len__(self):
        return len(self.fpaths0)

    def __getitem__(self, i):
        # Load slice into RAM
        img0 = np.array(Image.open(self.fpaths0[i]), dtype=np.float32)
        img1 = np.array(Image.open(self.fpaths1[i]), dtype=np.float32)

        # Transpose s.t. the LR axis is dimension 0
        img0 = img0.T
        img1 = img1.T

        # Interpolate
        img0 = resize(img0, dxyz=(1/self.scale, 1), order=5)
        img1 = resize(img1, dxyz=(1/self.scale, 1), order=5)

        return img0, img1
