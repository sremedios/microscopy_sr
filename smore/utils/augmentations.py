import numpy as np

def add_noise(img, noise_level):
    '''
    First adds Poisson noise.
    Then adds uniform noise.
    '''

    poisson_noise = np.random.poisson(np.abs(img)).astype(np.float32)
    uniform_noise = np.random.uniform(low=0.0, high=noise_level, size=img.shape).astype(np.float32)

    x = img + (poisson_noise * noise_level) + uniform_noise
    # re-center the mean
    x /= x.mean()
    x *= img.mean()

    return x


def flip_xy(a, b):
    """Randomly flips along x-axis, y-axis, or both, or neither."""
    if np.random.random() < 0.5:
        a = a[::-1, ...]
        b = b[::-1, ...]
    if np.random.random() < 0.5:
        a = a[:, ::-1, ...]
        b = b[:, ::-1, ...]
    return a.copy(), b.copy()


def get_intensity_shift(lr_vol):
    """
    We want to apply a randomly chosen uniform intensity shift
    to the entire LR and HR patch.
    ie: We simply add some scalar to all pixels in the patches.

    What is the maximum magnitude of this shift which is allowable?
    We'll choose to do 1 std difference between the through-plane
    and in-plane mean slice std.

    Requires the input to be a volume numpy with 3 dimensions.
    """
    stds = [[] for _ in range(3)]

    for i in range(lr_vol.shape[0]):
        stds[0].append(lr_vol[i, :, :].std())

    for i in range(lr_vol.shape[1]):
        stds[1].append(lr_vol[:, i, :].std())

    for i in range(lr_vol.shape[2]):
        stds[2].append(lr_vol[:, :, i].std())

    # get means of each of these
    mean_stds = list(map(lambda x: np.array(x).mean(), stds))

    # get maximum mean magnitude difference
    a = np.abs(mean_stds[0] - mean_stds[1])
    b = np.abs(mean_stds[1] - mean_stds[2])
    c = np.abs(mean_stds[0] - mean_stds[2])

    return max(a, max(b, c))


def apply_intensity_shift(x, y, val):
    """
    Roll a random number uniformly between [-`val`, `val`]
    and element-wise sum with `x` and `y`
    """
    # Flip a coin
    if np.random.random() < 0.5:
        v = np.random.uniform(-val, val)
        return x + v, y + v
    return x, y
