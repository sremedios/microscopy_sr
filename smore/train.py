import argparse
import sys
from pathlib import Path
import time

import numpy as np
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm

from .models.net import Net
from .models.edsr import EDSR
from .models.losses import l1_sobel_loss
from .utils.train_loader import TrainSet
from .utils.lsfm_train_loader import TrainSet as LSFMTrainSet
from .utils.degrade import WINDOW_OPTIONS

# Optimize torch
# noinspection PyUnresolvedReferences
torch.backends.cudnn.benchmark = True


def main(args=None):
    #################### ARGUMENTS ####################
    parser = argparse.ArgumentParser()
    parser.add_argument('--in-fpath', type=str)
    parser.add_argument('--weight-dir', type=str, required=True)
    parser.add_argument('--acq-dim', type=int, choices=[2, 3], default=2)
    parser.add_argument('--gpu-id', type=int, default=0)
    parser.add_argument('--interp-order', type=int, default=5)
    parser.add_argument('--n-rots', type=int, default=3)
    parser.add_argument('--pad-amt', type=int, default=16)
    parser.add_argument("--n-patches", type=int)
    parser.add_argument('--n-filters', type=int, default=256)
    parser.add_argument('--batch-size', type=int, default=16)
    parser.add_argument('--patch-size', type=int, default=32)
    parser.add_argument('--n-layers', type=int, default=32)
    parser.add_argument('--kernel-size', type=int, default=3)
    parser.add_argument('--lr-axis', type=int)
    parser.add_argument('--slice-thickness', type=float)
    parser.add_argument('--slice-spacing', type=float)
    parser.add_argument('--blur-kernel', type=str,
                        choices=WINDOW_OPTIONS, default='gaussian')
    parser.add_argument('--blur-kernel-file', type=str)
    parser.add_argument('--no-init-interp', action='store_true', default=False)
    parser.add_argument('--no-weighted-sampling',
                        action='store_true', default=False)
    parser.add_argument('--learning-rate', type=float, default=1e-4)
    parser.add_argument('--modality', type=str, default='MRI')
    parser.add_argument('--pretrain-weight-dir', type=str)
    parser.add_argument('--verbose', action='store_true', default=False)
    parser.add_argument('--no-augment', action='store_true', default=False)
    parser.add_argument('--model-type', type=str, required=True)
    parser.add_argument('--iteration', type=int, default=1)
    parser.add_argument('--lsfm-dir', type=str)
    parser.add_argument('--inplane-res', type=float)

    args = parser.parse_args(args if args is not None else sys.argv[1:])

    acq_res = (args.inplane_res, args.inplane_res, args.slice_thickness)

    if args.no_init_interp and args.slice_spacing is None:
        raise ValueError(
            'Image is already interpolated but slice spacing is not specified.')

    if args.in_fpath is not None and not Path(args.in_fpath).exists():
        raise ValueError('Input image path does not exist.')
    if args.blur_kernel_file is not None and not Path(args.blur_kernel_file).exists():
        raise ValueError('Blur kernel file is specified but does not exist.')

    print('{} BEGIN TRAINING {}'.format('=' * 20, '=' * 20))

    if args.n_patches is None:
        # If n_patches unspecified and using pretrained weights, don't train so long
        args.n_patches = 32000 if args.pretrain_weight_dir is not None else 320000
    patch_size = (args.patch_size, args.patch_size, 1)
    weight_dir = Path(args.weight_dir)
    gpu_device = torch.device(f'cuda:{args.gpu_id}')

    if not weight_dir.exists():
        weight_dir.mkdir(parents=True)

    if args.iteration > 1:
        init_interp = False
    else:
        init_interp = not args.no_init_interp

    #################### LOAD AND PROCESS DATA ####################

    if args.modality == 'MRI':
        train_ds = TrainSet(
            img_fpath=args.in_fpath,
            split='train',
            patch_size=patch_size,
            model_type=args.model_type,
            n_patches=args.n_patches,
            n_rots=args.n_rots,
            verbose=args.verbose,
            augment=not args.no_augment,
            lr_axis=args.lr_axis,
            slice_thickness=args.slice_thickness,
            slice_spacing=args.slice_spacing,
            blur_kernel=args.blur_kernel,
            blur_kernel_file=args.blur_kernel_file,
            acq_dim=args.acq_dim,
            init_interp=init_interp,
            interp_order=args.interp_order,
            weighted_sampling=not args.no_weighted_sampling,
            iteration=args.iteration,
        )

        val_ds = TrainSet(
            img_fpath=args.in_fpath,
            split='val',
            patch_size=patch_size,
            model_type=args.model_type,
            n_patches=args.n_patches//500,  # val set is 5% the size of train set
            n_rots=args.n_rots,
            verbose=args.verbose,
            augment=not args.no_augment,
            lr_axis=args.lr_axis,
            slice_thickness=args.slice_thickness,
            slice_spacing=args.slice_spacing,
            blur_kernel=args.blur_kernel,
            blur_kernel_file=args.blur_kernel_file,
            acq_dim=args.acq_dim,
            init_interp=init_interp,
            interp_order=args.interp_order,
            weighted_sampling=not args.no_weighted_sampling,
            iteration=args.iteration,
        )
    elif args.modality == 'LSFM':
        train_ds = LSFMTrainSet(
            img_dir=args.lsfm_dir,
            patch_size=patch_size,
            slice_spacing=args.slice_spacing,
            pad_amt=args.pad_amt,
            max_intensity_shift=100,
            n_patches=args.n_patches,
            augment=not args.no_augment,
            acq_res=acq_res,
        )

        val_ds = LSFMTrainSet(
            img_dir=args.lsfm_dir,
            split='val',
            patch_size=patch_size,
            slice_spacing=args.slice_spacing,
            pad_amt=args.pad_amt,
            max_intensity_shift=100,
            n_patches=args.n_patches//500,
            augment=not args.no_augment,
            acq_res=acq_res,
        )

    '''
    if args.model_type == 'EDSR':
        modes = ['AA', 'SR']
    else:
        modes = ['SR']
    '''
    modes = ['SR']

    for mode in modes:
        train_ds.set_mode(mode)
        val_ds.set_mode(mode)
        data_loader = DataLoader(
            train_ds,
            batch_size=args.batch_size,
            shuffle=False,  # Dataset automatically shuffles
            pin_memory=True,
            num_workers=8,
        )

        val_data_loader = DataLoader(
            val_ds,
            batch_size=args.batch_size,
            shuffle=False,  # Dataset automatically shuffles
            pin_memory=True,
            num_workers=8,
        )

        #################### MODEL SETUP ####################
        train_st = time.time()

        if args.model_type == 'EDSR':
            model = EDSR(
                n_channels=1,
                patch_size=args.patch_size,
                kernel_size=args.kernel_size,
                n_resblocks=int((args.n_layers - 2) / 2),
                filters=args.n_filters,
                padding_mode='zeros',
                min_clip=0,
            )

        else:
            model = Net(
                n_channels=1,
                patch_size=args.patch_size,
                kernel_size=args.kernel_size,
                n_layers=args.n_layers,
                filters=args.n_filters,
            )

        # Transfer learn from previous iteration
        if args.iteration > 1:
            prev_weight_dir = Path(str(weight_dir).replace(
                f"iter{args.iteration:03d}", f"iter{args.iteration - 1:03d}"))
            weight_fpath = sorted(
                filter(lambda x: mode in x.name, prev_weight_dir.iterdir()))[-1]
            print('*** Transfering weights from {} ***'.format(weight_fpath))
            checkpoint = torch.load(weight_fpath)
            model.load_state_dict(checkpoint['model'])

        if args.pretrain_weight_dir is not None:
            print('*** Loading pre-trained weights ***')
            checkpoint = torch.load(Path(args.pretrain_weight_dir) /
                                    '{}_{}_Net.h5'.format(mode, args.modality))
            model.load_state_dict(checkpoint['model'])
        model.to(gpu_device)

        opt = torch.optim.AdamW(model.parameters(), lr=args.learning_rate)
        #N_STEPS = args.n_patches // args.batch_size
        # scheduler = torch.optim.lr_scheduler.OneCycleLR(
        # optimizer=opt,
        # max_lr=args.learning_rate,
        # total_steps=N_STEPS+1,
        # cycle_momentum=True,
        # )
        # opt.step()

        # scaler = torch.cuda.amp.GradScaler()
        loss_obj = l1_sobel_loss

        convergence_counter = 0
        if args.modality == 'MRI':
            convergence_limit = 10
            epsilon = 1e-2
        elif args.modality == 'LSFM':
            # LSFM trains more
            convergence_limit = 100
            epsilon = 1e-4

        #################### TRAIN ####################
        print('\n{} TRAINING {} NETWORK {}\n'.format('=' * 20, mode, '=' * 20))

        with tqdm(total=args.n_patches) as pbar:
            pbar_dict = {
                'loss': np.finfo(np.float32).max,
                'val_loss': np.finfo(np.float32).max,
            }
            for batch_num, (xs, ys) in enumerate(data_loader):

                opt.zero_grad()

                ys_hat = model(xs.to(gpu_device))
                loss = loss_obj(ys_hat, ys.to(gpu_device))

                loss.backward()
                opt.step()

                # Progress bar dict update
                pbar_dict['loss'] = loss.detach().cpu().numpy()

                #################### VALIDATION ####################
                if (batch_num) % 100 == 0:  # check every 100 steps
                    val_loss = 0
                    for xs, ys in val_data_loader:
                        ys_hat = model(xs.to(gpu_device))
                        batch_val_loss = loss_obj(ys_hat, ys.to(gpu_device))
                        batch_val_loss = batch_val_loss.detach().cpu().numpy()
                        # sum up val loss across all batches in val set
                        val_loss += batch_val_loss

                    # take the mean
                    val_loss = val_loss / len(val_ds) * args.batch_size

                    # terminate if the new val loss did not improve enough
                    cond_no_improvement = val_loss > pbar_dict['val_loss']
                    condition_improvement = val_loss < pbar_dict['val_loss']
                    cond_insufficient_improvement = condition_improvement \
                        and np.abs(val_loss - pbar_dict['val_loss']) < epsilon

                    if cond_no_improvement or cond_insufficient_improvement:
                        convergence_counter += 1
                    if condition_improvement:
                        # reset convergence counter
                        convergence_counter = 0
                        # Update with new best weight value
                        pbar_dict['val_loss'] = val_loss
                        # save weights on improvement
                        weight_path = weight_dir / \
                            f'step_{batch_num:06d}_val-loss_{val_loss:.2f}_{mode}_weights.h5'
                        torch.save(
                            {'model': model.state_dict()}, str(weight_path))
                    if convergence_counter >= convergence_limit:
                        break

                # Progress bar update
                pbar.set_postfix({k: f'{v:.2f}' for k, v in pbar_dict.items()})
                pbar.update(args.batch_size)

        train_en = time.time()
        print('\tElapsed time to finish {} training: {:.4f}s'.format(
            mode, train_en - train_st))

    print('{} END TRAINING {}'.format('=' * 20, '=' * 20))
