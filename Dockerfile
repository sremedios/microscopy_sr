FROM registry.gitlab.com/iacl/neurobuilds/pytorch:1.8.1-py3.8-cuda10.2-ubuntu18.04

COPY weights_pytorch /weights
COPY setup.py COMMIT_INFO.txt /src/
COPY smore /src/smore
COPY scripts /src/scripts

RUN apt-get update && \
    apt-get -y install git && \
    pip3 install /src && \
    rm -rf /src && \
    mkdir /inputs /outputs
ENV PATH /opt/conda/bin:/src/scripts:${PATH}
