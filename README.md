# SMORE: Synthetic Multi-Orientation Resolution Enhancement | [Paper](https://doi.org/10.1109/TMI.2020.3037187)
SMORE is a self super-resolution method; no outside training data is required. There is an option to provide pretrained weights (described below). The only required input is the image itself and the output directory.

## Installation
From the root project directory, run:

 ```pip install .```

 Package requirements are automatically handled. To see a list of requirements, see `setup.py` L98-103.
 
## Basic Usage
 From the project directory, call:

 ```./scripts/run_smore.sh ${PATH_TO_INPUT_VOLUME} ${PATH_TO_OUTPUT_DIRECTORY}```

## Advanced Arguments
 Beyond basic usage, the commands `smore-train` and `smore-test` accept several command line arguments. The file `scripts/run_smore.sh` can be modified as necessary to pass these arguments appropriately.

### `smore-train`

|Option|Description|Default|
|---|---|---|
|`--in-fpath`|Path to the `.nii` or `.nii.gz` file to super-resolve|`REQUIRED`|
|`--weight-dir`|Path to the directory to output weights|`REQUIRED`|
|`--acq-dim`|Acquisition dimensionality. `2` for 2D acquisitions and `3` for 3D acquistions. 3D IS CURRENTLY NOT SUPPORTED.|`2`|
|`--gpu-id`|Index of the GPU to run on|`0`|
|`--interp-order`|Order of the spline used for all interpolations.|`5`|
|`--n-rots`|Number of training rotations to use, including the 0 degree rotation.|`3`|
|`--n-iters`|Number of training patches seen before halting training. If not specified defaults to `32000` if using pre-trained weights and `320000` otherwise. |`SEE DESCRIPTION`|
|`--n-filters`|Number of filters in the EDSR model. Deviating from the default breaks compatibility with pre-trained weights.|`256`|
|`--batch-size`|Batch size used for training|`32`|
|`--patch-size`|The patch size used for training. This single integer determines the square patch size|`32`|
|`--n-resblocks`|Number of residual blocks in the EDSR model. Deviating from the default breaks compatibility with the pre-trained weights.|`32`|
|`--kernel-size`|Size of the square kernel used in convolutional layers in the EDSR model. Deviating from the default breaks compatibility with the pre-trained weights.|`3`|
|`--lr-axis`|Index of the axis on which the input volume is LR. Automatically inferred from header if absent.|`None`|
|`--slice-spacing`|Spacing between voxels in the through plane direction. This controls the downsampling step during aliasing. Defaults to the maximum voxel size.|`SEE DESCRIPTION`|
|`--slice-thickness`|Thickness of voxels in the through plane direction. This controls the FWHM of the blur kernel. Defaults to the `--slice-thickness` value.|`SEE DESCRIPTION`|
|`--blur-kernel`|Shape of the generated blur kernel. Choices are: `blackman`, `hann`, `hamming`, `gaussian`, `cosine`, `parzen`. value.|`gaussian`|
|`--blur-kernel-file`|A numpy file defining a custom kernel to implement during training. Overrides all other kernel features (`--slice-thickness`, `--blur-kernel`).|`None`|
|`--no-init-interp`|Do not interpolate the volume in the through-plane direction before training.||
|`--learning-rate`|Learning rate used during training.|`1e-4`|
|`--modality`|Modality of the input volume. Currently supported: `MRI` and `CT`.|`MRI`|
|`--pretrain-weight-dir`|Path to the directory containing pre-trained weights if used for transfer learning|`None`|
|`--verbose`|Print processing to terminal during training.||
|`--no-augment`|Do not include augmentations during training.||

### `smore-test`
|Option|Description|Default|
|---|---|---|
|`--in-fpath`|Path to the `.nii` or `.nii.gz` file to super-resolve|`REQUIRED`|
|`--out-fpath`|Path and name of the desired super-resolved file|`REQUIRED`|
|`--weight-dir`|Path to the directory to the trained weights|`REQUIRED`|
|`--gpu-id`|Index of the GPU to run on|`0`|
|`--interp-order`|Order of the spline used for all interpolations.|`5`|
|`--batch-size`|Batch size used for testing. Since we test on full slices, we recommend this value to be small.|`1`|
|`--n-resblocks`|Number of residual blocks in the EDSR model. Must be the same value used during training to obtain the weights provided.|`32`|
|`--n-rots`|Number of test-time rotations to use, including the 0 degree rotation.|`2`|
|`--n-filters`|Number of filters in the EDSR model. Must be the same value used during training to obtain the weights provided.|`256`|
|`--kernel-size`|Size of the square kernel used in convolutional layers in the EDSR model. Must be the same value used during training to obtain the weights provided.|`3`|
|`--no-init-interp`|Do not interpolate the volume in the through-plane direction before training.||
|`--acq-dim`|Acquisition dimensionality. `2` for 2D acquisitions and `3` for 3D acquistions. 3D IS CURRENTLY NOT SUPPORTED.|`2`|
|`--lr-axis`|Index of the axis on which the input volume is LR. Automatically inferred from header if absent.|`None`|


## Citations
If this work is useful to you or your project, please consider citing our relevant papers:

```
C. Zhao, B. E. Dewey, D. L. Pham, P. A. Calabresi, D. S. Reich and J. L. Prince,
"SMORE: A Self-Supervised Anti-Aliasing and Super-Resolution Algorithm for MRI 
Using Deep Learning," in IEEE Transactions on Medical Imaging, vol. 40, no. 3, 
pp. 805-817, March 2021, doi: 10.1109/TMI.2020.3037187.
```

```
C. Zhao, M. Shao, A. Carass, H. Li, B. E. Dewey, L. M. Ellingsen, J. Woo, 
M. A. Guttman, A. M. Blitz, M. Stone, P. A. Calabresi, H. Halperin, and J. L. Prince, 
“Applications of a deep learning method for anti-aliasing and super-resolution in 
MRI”, Magnetic Resonance Imaging, 64:132-141, 2019.
```

```
Zhao C., Son S., Kim Y., Prince J.L. (2019) iSMORE: An Iterative Self 
Super-Resolution Algorithm. In: Burgos N., Gooya A., Svoboda D. (eds) Simulation 
and Synthesis in Medical Imaging. SASHIMI 2019. Lecture Notes in Computer Science, 
vol 11827. Springer, Cham. https://doi.org/10.1007/978-3-030-32778-1_14
```

```
Zhao C. et al. (2018) A Deep Learning Based Anti-aliasing Self Super-Resolution 
Algorithm for MRI. In: Frangi A., Schnabel J., Davatzikos C., Alberola-López C., 
Fichtinger G. (eds) Medical Image Computing and Computer Assisted Intervention – 
MICCAI 2018. MICCAI 2018. Lecture Notes in Computer Science, vol 11070. Springer, 
Cham. https://doi.org/10.1007/978-3-030-00928-1_12
```

```
C. Zhao, A. Carass, B. E. Dewey and J. L. Prince, "Self super-resolution for
magnetic resonance images using deep networks," 2018 IEEE 15th International 
Symposium on Biomedical Imaging (ISBI 2018), 2018, pp. 365-368, 
doi: 10.1109/ISBI.2018.8363594.
```
