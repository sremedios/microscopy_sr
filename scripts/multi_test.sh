#!/usr/bin/bash

IN_FPATH=$1
OUT_DIR=$2
MODEL_TYPE=$3
GPU_ID=$4

APPLY_TRAIN="false"
APPLY_TEST="true"
USE_CHUNKS="true"
INTERP_ORDER=5

if [ ${USE_CHUNKS} = "true" ]
then
    CHUNK_SIZE=64
    CHUNK_STR="${CHUNK_SIZE}chunks"
else
    CHUNK_SIZE=0
    CHUNK_STR="fullslice"
fi

if [ ${MODEL_TYPE} = "EDSR" ]
then
    PATCH_SIZE=32
    N_LAYERS=66
    N_FILTERS=256
else
    PATCH_SIZE=64
    N_LAYERS=16
    N_FILTERS=256
fi

PAD_AMT=16
BATCH_SIZE=32
TEST_BATCH_SIZE=12

EXPR_STR="${MODEL_TYPE}_smoreCE_filters=${N_FILTERS}_ps=${PATCH_SIZE}_layers=${N_LAYERS}"

IN_FNAME=$(basename "${IN_FPATH}")
IN_ID=${IN_FNAME%.nii*}
IN_EXT=${IN_FNAME#${IN_ID}}

WEIGHT_DIR="${OUT_DIR}/${IN_ID}/${EXPR_STR}/weights"

for i in 0000 1000 2000 3000 4000 5000 6000 7000 8000 9000
do
    OUT_FPATH="${OUT_DIR}/${IN_ID}/ADNI4x_step=${i}_${CHUNK_STR}_${EXPR_STR}${IN_EXT}" 
    smore-test \
        --in-fpath "${IN_FPATH}" \
        --out-fpath "${OUT_FPATH}" \
        --weight-dir "${WEIGHT_DIR}" \
        --weight-step ${i} \
        --batch-size ${TEST_BATCH_SIZE} \
        --patch-size ${PATCH_SIZE} \
        --n-layers ${N_LAYERS} \
        --n-filters ${N_FILTERS} \
        --chunk-size ${CHUNK_SIZE} \
        --pad-amt ${PAD_AMT} \
        --model-type ${MODEL_TYPE} \
        --interp-order ${INTERP_ORDER} \
        --gpu-id ${GPU_ID}
done
OUT_FPATH="${OUT_DIR}/${IN_ID}/ADNI4x_step=best_${CHUNK_STR}_AASR_${EXPR_STR}${IN_EXT}" 

smore-test \
    --in-fpath "${IN_FPATH}" \
    --out-fpath "${OUT_FPATH}" \
    --weight-dir "${WEIGHT_DIR}" \
    --batch-size ${TEST_BATCH_SIZE} \
    --patch-size ${PATCH_SIZE} \
    --n-layers ${N_LAYERS} \
    --n-filters ${N_FILTERS} \
    --chunk-size ${CHUNK_SIZE} \
    --pad-amt ${PAD_AMT} \
    --model-type ${MODEL_TYPE} \
    --interp-order ${INTERP_ORDER} \
    --gpu-id ${GPU_ID}
