import os
import sys
from pathlib import Path


downsample = int(sys.argv[1])
gpu_id = int(sys.argv[2])

periods = [32, 64, 96]

for period in periods:

    inp_dir = Path("../../data/simulated_lr")
    out_dir = Path("../../data/results/SR_simulated_phantom")
    fname = "circle_triangle_wave_period={}_{:02d}xLR.nii.gz".format(
        period, downsample)

    cmd = "./run_smore.sh {} {} {}".format(inp_dir / fname, out_dir, gpu_id)
    os.system(cmd)
