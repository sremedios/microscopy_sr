RES=$1
GPU_ID=$2
USE_CHUNKS=$3
CHUNK_SIZE=$4
MODEL_TYPE=$5
N_ITERS=1

RUN_SCRIPT="/ISFILE3/USERS/remediossw/smore/scripts/run_espreso_ismore.sh"

RES_STRING=`printf %02d $RES`

ADNI_DIR="/ISFILE3/USERS/remediossw/data/ADNI/ADNI_phantom_1mm_inplane_header_corrected"

IN_FPATH="${ADNI_DIR}/SUPERRES-ADNIPHANTOM_20200711_PHANTOM-T2-TSE-2D-CORONAL-PRE-ACQ1-${RES_STRING}mm_resampled.nii"
SLICE_THICKNESS=${RES}
SLICE_SPACING=${RES}
OUT_DIR="/ISFILE3/USERS/remediossw/data/results/smoreCE"

${RUN_SCRIPT} \
    ${IN_FPATH} \
    ${SLICE_THICKNESS} \
    ${SLICE_SPACING} \
    ${OUT_DIR} \
    ${GPU_ID} \
    ${MODEL_TYPE} \
    ${USE_CHUNKS} \
    ${N_ITERS} \
    "ADNI" \
    ${CHUNK_SIZE}
