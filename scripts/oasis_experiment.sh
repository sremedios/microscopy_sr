GPU_ID=$1
MODEL_TYPE=$2
RES=$3

USE_CHUNKS=false
CHUNK_SIZE=0
N_ITERS=1


RUN_SCRIPT="/ISFILE3/USERS/remediossw/smore/scripts/run_espreso_ismore.sh"
ROOT_DIR="/ISFILE3/USERS/remediossw/data/OASIS3"
RES_STR=`printf %02d ${RES}`
DATA_DIR="${ROOT_DIR}/OASIS3_${RES_STR}x_aniso"
OUT_DIR="/ISFILE3/USERS/remediossw/data/results/smoreCE/OASIS3_${RES_STR}x"

SLICE_THICKNESS=${RES}
SLICE_SPACING=${RES}

for IN_FPATH in "${DATA_DIR}"/*; do
    ${RUN_SCRIPT} \
        ${IN_FPATH} \
        ${SLICE_THICKNESS} \
        ${SLICE_SPACING} \
        ${OUT_DIR} \
        ${GPU_ID} \
        ${MODEL_TYPE} \
        ${USE_CHUNKS} \
        ${N_ITERS} \
        "OASIS" \
        ${CHUNK_SIZE}
done
