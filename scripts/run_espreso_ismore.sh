IN_FPATH=$1
SLICE_THICKNESS=$2
SLICE_SPACING=$3
OUT_DIR=$4
GPU_ID=$5
MODEL_TYPE=$6
USE_CHUNKS=$7
N_ITERS=$8
DATASET=$9
CHUNK_SIZE=${10}

CUDA_VISIBLE_DEVICES=${GPU_ID}

res=`printf %02d ${SLICE_THICKNESS}`

# Process ID of input file
IN_FNAME=$(basename "${IN_FPATH}")
IN_ID=${IN_FNAME%.nii*}
IN_EXT=${IN_FNAME#${IN_ID}}
ID_DIR="${OUT_DIR}/${IN_ID}"
mkdir -p ${ID_DIR}

##### ESPRESO CODE #####
ESPRESO_DIR="${ID_DIR}/ESPRESO2_output"
cd "/ISFILE3/USERS/remediossw/espreso2"
train.py -i ${IN_FPATH} -o ${ESPRESO_DIR}
KERNEL_FILE="${ESPRESO_DIR}/result.npy"

cd "/ISFILE3/USERS/remediossw/smore"
##### ISMORE CODE #####
APPLY_TRAIN="true"
APPLY_TEST="true"
INTERP_ORDER=5

if [ ${USE_CHUNKS} = "true" ]
then
    #CHUNK_SIZE=32
    #CHUNK_SIZE=134
    CHUNK_STR="${CHUNK_SIZE}chunks"
else
    CHUNK_SIZE=0
    CHUNK_STR="fullslice"
fi

if [ ${MODEL_TYPE} = "EDSR" ]
then
    PATCH_SIZE=32
    #PAD_AMT=32
    N_LAYERS=66
    N_FILTERS=256
else
    PATCH_SIZE=64
    #PAD_AMT=16
    N_LAYERS=16
    N_FILTERS=256
fi

# Network hyperparameters
LR_AXIS=2
N_ROTS=3
BATCH_SIZE=32
TEST_BATCH_SIZE=12
N_PATCHES=320000

EXPR_STR="${MODEL_TYPE}_espresso_smore"
ORIG_IN_FPATH=${IN_FPATH}

for ((iteration=1; iteration<=${N_ITERS}; iteration++)); do
	iter=`printf %03d $iteration`

    WEIGHT_DIR="${OUT_DIR}/${IN_ID}/iter${iter}_${EXPR_STR}/weights"
    # For subsequent iterations, take the previous output as input
    if [ $iteration != "1" ]; then
        IN_FPATH=${OUT_FPATH}
        # cut the number of training steps to 10% from scratch
        N_PATCHES=32000
    fi

    # update new output fpath
    OUT_FPATH="${OUT_DIR}/${IN_ID}/iter${iter}_${DATASET}${res}x_${CHUNK_STR}_${EXPR_STR}${IN_EXT}" 
    if [ ${APPLY_TRAIN} = "true" ] ; then
        echo "Training on ${IN_FPATH}..."
        smore-train \
            --in-fpath "${IN_FPATH}" \
            --weight-dir "${WEIGHT_DIR}" \
            --blur-kernel-file ${KERNEL_FILE} \
            --batch-size ${BATCH_SIZE} \
            --patch-size ${PATCH_SIZE} \
            --n-rots ${N_ROTS} \
            --n-patches ${N_PATCHES} \
            --n-layers ${N_LAYERS} \
            --lr-axis ${LR_AXIS} \
            --slice-thickness ${SLICE_THICKNESS} \
            --slice-spacing ${SLICE_SPACING} \
            --n-filters ${N_FILTERS} \
            --model-type ${MODEL_TYPE} \
            --gpu-id ${GPU_ID} \
            --interp-order ${INTERP_ORDER} \
            --iteration ${iteration} \
            --verbose
    fi

    if [ ${APPLY_TEST} = "true" ] ; then
        echo "Applying to ${ORIG_IN_FPATH}..."
        smore-test \
            --in-fpath "${ORIG_IN_FPATH}" \
            --out-fpath "${OUT_FPATH}" \
            --weight-dir "${WEIGHT_DIR}" \
            --batch-size ${TEST_BATCH_SIZE} \
            --n-layers ${N_LAYERS} \
            --n-filters ${N_FILTERS} \
            --lr-axis ${LR_AXIS} \
            --n-rots 2 \
            --patch-size ${PATCH_SIZE} \
            --chunk-size ${CHUNK_SIZE} \
            --model-type ${MODEL_TYPE} \
            --interp-order ${INTERP_ORDER} \
            --gpu-id ${GPU_ID}
    fi
            #--pad-amt ${PAD_AMT} \
done

