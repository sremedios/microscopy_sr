GPU_ID=$4
BATCH_SIZE=32
TEST_BATCH_SIZE=12

IN_FPATH="${1}"
IN_FNAME=$(basename "${IN_FPATH}")
IN_ID=${IN_FNAME%.nii*}
IN_EXT=${IN_FNAME#${IN_ID}}
OUT_DIR="${2}"
OUT_FPATH="${OUT_DIR}/${IN_ID}/${IN_ID}_smore3${IN_EXT}" 

WEIGHT_DIR="${3}"

smore-test \
    --in-fpath "${IN_FPATH}" \
    --out-fpath "${OUT_FPATH}" \
    --weight-dir "${WEIGHT_DIR}" \
    --batch-size ${TEST_BATCH_SIZE} \
    --gpu-id ${GPU_ID}
