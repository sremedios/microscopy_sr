LSFM_DIR=$1
OUT_DIR=$2
GPU_ID=$3

TEST_DIR_0=$4
TEST_DIR_1=$5
OUT_DIR_0=$6
OUT_DIR_1=$7

PARTITION=$8

MODEL_TYPE="valid"
USE_CHUNKS="true"
N_ITERS=1
DATASET="LSFM"
CHUNK_SIZE=64
APPLY_TRAIN="false"
APPLY_TEST="true"
INTERP_ORDER=5

# We know this from asking the technician
INPLANE_RES=0.411
SLICE_THICKNESS=2.2
SLICE_SPACING=1

CUDA_VISIBLE_DEVICES=${GPU_ID}

# Process ID of input file
IN_ID=$(basename "${LSFM_DIR}")
ID_DIR="${OUT_DIR}/${IN_ID}"
mkdir -p ${ID_DIR}


if [ ${USE_CHUNKS} = "true" ]
then
    #CHUNK_SIZE=32
    #CHUNK_SIZE=134
    CHUNK_STR="${CHUNK_SIZE}chunks"
else
    CHUNK_SIZE=0
    CHUNK_STR="fullslice"
fi

if [ ${MODEL_TYPE} = "EDSR" ]
then
    PATCH_SIZE=32
    #PAD_AMT=32
    N_LAYERS=66
    N_FILTERS=256
else
    PATCH_SIZE=64
    PAD_AMT=16
    N_LAYERS=16
    N_FILTERS=256
fi

# Network hyperparameters
LR_AXIS=2
N_ROTS=3
BATCH_SIZE=32
TEST_BATCH_SIZE=12
N_PATCHES=500000

EXPR_STR="${MODEL_TYPE}_smore_thresh-0.9_p-64_n-500k_denoise"
ORIG_IN_FPATH=${IN_FPATH}

WEIGHT_DIR="${OUT_DIR}/${IN_ID}/${EXPR_STR}/weights"

if [ ${APPLY_TRAIN} = "true" ] ; then
    echo "Training on ${LSFM_DIR}..."
    smore-train \
        --lsfm-dir ${LSFM_DIR} \
        --weight-dir ${WEIGHT_DIR} \
        --batch-size ${BATCH_SIZE} \
        --patch-size ${PATCH_SIZE} \
        --n-rots ${N_ROTS} \
        --n-patches ${N_PATCHES} \
        --n-layers ${N_LAYERS} \
        --lr-axis ${LR_AXIS} \
        --n-filters ${N_FILTERS} \
        --model-type ${MODEL_TYPE} \
        --gpu-id ${GPU_ID} \
        --interp-order ${INTERP_ORDER} \
        --inplane-res ${INPLANE_RES} \
        --slice-thickness ${SLICE_THICKNESS} \
        --slice-spacing ${SLICE_SPACING} \
        --pad-amt ${PAD_AMT} \
        --modality "LSFM" \
        --verbose
fi

if [ ${APPLY_TEST} = "true" ] ; then
    lsfm-test \
        --in-dir0 ${TEST_DIR_0} \
        --in-dir1 ${TEST_DIR_1} \
        --out-dir0 ${OUT_DIR_0} \
        --out-dir1 ${OUT_DIR_1} \
        --weight-dir "${WEIGHT_DIR}" \
        --n-layers ${N_LAYERS} \
        --n-filters ${N_FILTERS} \
        --patch-size ${PATCH_SIZE} \
        --chunk-size ${CHUNK_SIZE} \
        --interp-order ${INTERP_ORDER} \
        --slice-spacing ${SLICE_SPACING} \
        --inplane-res ${INPLANE_RES} \
        --gpu-id ${GPU_ID} \
        --partition ${PARTITION}
fi
