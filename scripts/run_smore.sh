#!/usr/bin/bash

IN_FPATH=$1
OUT_DIR=$2
MODEL_TYPE=$3
GPU_ID=$4

APPLY_TRAIN="false"
APPLY_TEST="true"
USE_CHUNKS="false"
INTERP_ORDER=5

if [ ${USE_CHUNKS} = "true" ]
then
    CHUNK_SIZE=64
    #CHUNK_SIZE=134
    CHUNK_STR="${CHUNK_SIZE}chunks"
else
    CHUNK_SIZE=0
    CHUNK_STR="fullslice"
fi

if [ ${MODEL_TYPE} = "EDSR" ]
then
    PATCH_SIZE=32
    N_LAYERS=66
    N_FILTERS=256
else
    PATCH_SIZE=64
    N_LAYERS=16
    N_FILTERS=256
fi

PAD_AMT=16
N_ROTS=3
BATCH_SIZE=8
TEST_BATCH_SIZE=12
#N_PATCHES=960000
N_PATCHES=320000

EXPR_STR="${MODEL_TYPE}_smoreCE-with-val_espreso_four-test-time-flips_filters=${N_FILTERS}_ps=${PATCH_SIZE}_layers=${N_LAYERS}"

IN_FNAME=$(basename "${IN_FPATH}")
IN_ID=${IN_FNAME%.nii*}
IN_EXT=${IN_FNAME#${IN_ID}}

KERNEL_FILE="${OUT_DIR}/${IN_ID}/ESPRESO2_output/result.npy"
WEIGHT_DIR="${OUT_DIR}/${IN_ID}/${EXPR_STR}/weights"
# WEIGHT_FPATH="${WEIGHT_DIR}/step_020800_val-loss_36.97_SR_weights.h5"

OUT_FPATH="${OUT_DIR}/${IN_ID}/ADNI4x_${CHUNK_STR}_${EXPR_STR}${IN_EXT}" 

if [ ${APPLY_TRAIN} = "true" ] ; then
    smore-train \
        --in-fpath "${IN_FPATH}" \
        --weight-dir "${WEIGHT_DIR}" \
        --blur-kernel-file ${KERNEL_FILE} \
        --batch-size ${BATCH_SIZE} \
        --patch-size ${PATCH_SIZE} \
        --n-rots ${N_ROTS} \
        --n-patches ${N_PATCHES} \
        --n-layers ${N_LAYERS} \
        --n-filters ${N_FILTERS} \
        --pad-amt ${PAD_AMT} \
        --model-type ${MODEL_TYPE} \
        --gpu-id ${GPU_ID} \
        --interp-order ${INTERP_ORDER} \
        --verbose
fi

#--weight-file "${WEIGHT_FPATH}" \
if [ ${APPLY_TEST} = "true" ] ; then
    smore-test \
        --in-fpath "${IN_FPATH}" \
        --out-fpath "${OUT_FPATH}" \
        --weight-dir "${WEIGHT_DIR}" \
        --batch-size ${TEST_BATCH_SIZE} \
        --n-layers ${N_LAYERS} \
        --n-filters ${N_FILTERS} \
        --patch-size ${PATCH_SIZE} \
        --chunk-size ${CHUNK_SIZE} \
        --pad-amt ${PAD_AMT} \
        --model-type ${MODEL_TYPE} \
        --interp-order ${INTERP_ORDER} \
        --gpu-id ${GPU_ID}
fi
exit
